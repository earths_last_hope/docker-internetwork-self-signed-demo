# docker-internetwork-self-signed-demo
Just a small example of how to set up docker-compose for two containers where one of them requires a self signed SSL certificate and the other needs to have it installed.

## Services

### httpsie
Spins up a really basic nginx website using a untrusted SSL certificate which is generated during image build.
If you really want so badly to see it in a browser then you need to add a ports binding for 443. Trust me, it's not that special :-)

### curly
Ubuntu based container that installs the certificate from httpsie, waits 10 seconds, makes request for the webpage from httpsie and keeps running forever unless it's stopped.

## How to run?
It's really that simple:
```bash
docker-compose up
```